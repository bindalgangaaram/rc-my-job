package coo.app.baseline.fax.definitions;
import com.ringcentral.definitions.ActivePermissionResource;

public class AuthProfileResource
{
    //
    public String uri;
    public  AuthProfileResource uri(String uri) {
        this.uri = uri;
        return this;
    }
    //
    public ActivePermissionResource[] permissions;
    public  AuthProfileResource permissions(ActivePermissionResource[] permissions) {
        this.permissions = permissions;
        return this;
    }
}
