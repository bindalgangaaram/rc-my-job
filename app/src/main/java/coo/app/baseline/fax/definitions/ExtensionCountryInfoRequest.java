package coo.app.baseline.fax.definitions;

public class ExtensionCountryInfoRequest
{
    // internal Identifier for country
    public String id;
    public ExtensionCountryInfoRequest id(String id) {
        this.id = id;
        return this;
    }
}
