package coo.app.baseline.fax.definitions;


public class UserBusinessHoursUpdateRequest
{
    // Schedule when an answering rule is applied
    public WeeklyScheduleInfo schedule;
    public UserBusinessHoursUpdateRequest schedule(WeeklyScheduleInfo schedule) {
        this.schedule = schedule;
        return this;
    }
}
