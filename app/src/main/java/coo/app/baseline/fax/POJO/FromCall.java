package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/30/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FromCall {

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("name")
    @Expose
    private String name;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

}