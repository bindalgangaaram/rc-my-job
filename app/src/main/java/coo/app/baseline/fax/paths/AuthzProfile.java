package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Check;

public class AuthzProfile extends Path {
    public AuthzProfile (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "authz-profile", id);
    }
    public com.ringcentral.paths.Check check()
    {
        return new Check(restClient, pathSegment, null);
    }
}
