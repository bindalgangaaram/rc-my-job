package coo.app.baseline.fax.definitions;


public class ForwardingInfoCreateRuleRequest
{
    // Information on a call forwarding rule
    public RuleInfoCreateRuleRequest[] rules;
    public ForwardingInfoCreateRuleRequest rules(RuleInfoCreateRuleRequest[] rules) {
        this.rules = rules;
        return this;
    }
}
