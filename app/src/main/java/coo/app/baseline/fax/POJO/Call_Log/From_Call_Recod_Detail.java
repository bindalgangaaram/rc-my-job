package coo.app.baseline.fax.POJO.Call_Log;

/**
 * Created by Android on 2/5/2018.
 */
 import java.io.Serializable;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;


public class From_Call_Recod_Detail implements Serializable
{

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("extensionNumber")
    @Expose
    private String extensionNumber;
    @SerializedName("location")
    @Expose
    private String location;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("device")
    @Expose
    private Device device;
    private final static long serialVersionUID = -5035721610507492714L;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getExtensionNumber() {
        return extensionNumber;
    }

    public void setExtensionNumber(String extensionNumber) {
        this.extensionNumber = extensionNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Device getDevice() {
        return device;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

}