package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.BulkAssign;

public class ParkLocations extends Path {
    public ParkLocations (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "park-locations", id);
    }
    public Users users()
    {
        return new Users(restClient, pathSegment, null);
    }
    public com.ringcentral.paths.BulkAssign bulkAssign()
    {
        return new BulkAssign(restClient, pathSegment, null);
    }
}
