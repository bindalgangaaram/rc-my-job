package coo.app.baseline.fax.definitions;

public class CompanyAnsweringRuleExtensionInfoRequest
{
    // Time in format hh:mm
    public String id;
    public CompanyAnsweringRuleExtensionInfoRequest id(String id) {
        this.id = id;
        return this;
    }
}
