package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.BulkAssign;
import com.ringcentral.paths.Devices;

public class PagingOnlyGroups extends Path {
    public PagingOnlyGroups (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "paging-only-groups", id);
    }
    public Users users()
    {
        return new Users(restClient, pathSegment, null);
    }
    public com.ringcentral.paths.Devices devices()
    {
        return new Devices(restClient, pathSegment, null);
    }
    public BulkAssign bulkAssign()
    {
        return new BulkAssign(restClient, pathSegment, null);
    }
}
