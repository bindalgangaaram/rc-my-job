package coo.app.baseline.fax;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;


public class Register extends AppCompatActivity {
    Button btn_registation;
    EditText reg_user_name, reg_phone_no, reg_password, reg_email;
    String reg_name, reg_phn_no, reg_pass, _reg_email;
    private DatabaseReference mDatabase;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        btn_registation = (Button) findViewById(R.id.btn_registation);
        reg_user_name = (EditText) findViewById(R.id.reg_user_name);
        reg_phone_no = (EditText) findViewById(R.id.reg_phone_no);
        reg_password = (EditText) findViewById(R.id.reg_password);
        reg_email = (EditText) findViewById(R.id.reg_email);

        btn_registation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                reg_name = reg_user_name.getText().toString();
                reg_phn_no = reg_phone_no.getText().toString();
                reg_pass = reg_password.getText().toString();
                _reg_email = reg_email.getText().toString();

                 mDatabase = FirebaseDatabase.getInstance().getReference("email_singup");
                String id = mDatabase.push().getKey();
                Email_SINGUP email_singup;
                email_singup = new Email_SINGUP(id,_reg_email,reg_name,reg_phn_no,reg_pass);
                mDatabase.child(id).setValue(email_singup);

            }
        });
    }
}
