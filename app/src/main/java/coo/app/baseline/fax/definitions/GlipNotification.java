package coo.app.baseline.fax.definitions;

import com.ringcentral.definitions.GlipPost;

public class GlipNotification {
    public String uuid;
    public String event;
    public String timestamp;
    public String subscriptionId;
    public GlipPost body;
}
