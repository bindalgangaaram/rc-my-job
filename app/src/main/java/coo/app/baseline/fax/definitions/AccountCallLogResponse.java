package coo.app.baseline.fax.definitions;
import com.ringcentral.definitions.CallLogRecord;
import com.ringcentral.definitions.NavigationInfo;
import com.ringcentral.definitions.PagingInfo;

public class AccountCallLogResponse
{
    // List of call log records
    public CallLogRecord[] records;
    public  AccountCallLogResponse records(CallLogRecord[] records) {
        this.records = records;
        return this;
    }
    // Information on navigation
    public NavigationInfo navigation;
    public  AccountCallLogResponse navigation(NavigationInfo navigation) {
        this.navigation = navigation;
        return this;
    }
    // Information on paging
    public PagingInfo paging;
    public  AccountCallLogResponse paging(PagingInfo paging) {
        this.paging = paging;
        return this;
    }
}
