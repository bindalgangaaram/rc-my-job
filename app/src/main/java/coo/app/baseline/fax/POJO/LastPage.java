package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/25/2018.
 */




import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LastPage {

    @SerializedName("uri")
    @Expose
    private String uri;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

}