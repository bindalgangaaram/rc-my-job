package coo.app.baseline.fax.definitions;

public class UnconditionalForwardingResource
{
    //
    public String phoneNumber;
    public UnconditionalForwardingResource phoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }
}
