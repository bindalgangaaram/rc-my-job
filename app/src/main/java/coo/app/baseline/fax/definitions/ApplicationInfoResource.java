package coo.app.baseline.fax.definitions;
import com.ringcentral.definitions.ApplicationResource;

public class ApplicationInfoResource
{
    //
    public String clientId;
    public  ApplicationInfoResource clientId(String clientId) {
        this.clientId = clientId;
        return this;
    }
    //
    public  ApplicationResource application;
    public  ApplicationInfoResource application(ApplicationResource application) {
        this.application = application;
        return this;
    }
}
