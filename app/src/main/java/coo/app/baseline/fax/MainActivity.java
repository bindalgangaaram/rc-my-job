package coo.app.baseline.fax;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.ringcentral.RestException;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import coo.app.baseline.fax.POJO.Attachment;
import coo.app.baseline.fax.POJO.Call_Log.Call_Log;
import coo.app.baseline.fax.POJO.Conversation;
import coo.app.baseline.fax.POJO.From;
import coo.app.baseline.fax.POJO.FromCall;
import coo.app.baseline.fax.POJO.Record;
import coo.app.baseline.fax.POJO.RecordCall;
import coo.app.baseline.fax.POJO.To;
import coo.app.baseline.fax.POJO.ToCall;
import coo.app.baseline.fax.comm.ObjectCaster;
import okhttp3.ResponseBody;

import static junit.framework.Assert.assertNotNull;


public class MainActivity extends AppCompatActivity {

    Button btntoken, btnfax, btnemail, btnchat, btncontactinfo, btnCall, _btn_red_call_log;
    RestClient restClient;
    private DatabaseReference mDatabase;
    static List<Record> recordList = new ArrayList<>();
    static List<RecordCall> recordListCall = new ArrayList<>();
    static List<Call_Log> call_logList = new ArrayList<>();
    String pho, codeWithOutPlus, codeWithPlus, phonCodeWithOutPlus, phonCodeWithPlus;
    String call_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        SessionManagement sessionManagement = new SessionManagement(getApplicationContext());
        sessionManagement.checkLogin();

        Intent intent = getIntent();
        pho = intent.getStringExtra("phonenumber");
        codeWithOutPlus = intent.getStringExtra("codeWithOutPlus");
        codeWithPlus = intent.getStringExtra("codeWithPlus");
        phonCodeWithOutPlus = codeWithOutPlus + pho;
        phonCodeWithPlus = codeWithPlus + pho;
        btnCall = (Button) findViewById(R.id._btnCall);
        _btn_red_call_log = (Button) findViewById(R.id._btn_red_call_log);
        restClient = new RestClient(Config.appKey, Config.appSecret, Config.server);
        Send_Token send_token = new Send_Token();
        send_token.execute();

        btnCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Get_Messages get_messages = new Get_Messages();
                get_messages.execute();
            }
        });
        _btn_red_call_log.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Get_Call_Detail get_call_detail = new Get_Call_Detail();
                get_call_detail.execute();
            }
        });
    }

//    public class Get_Call_log extends AsyncTask<Object, Object, String> {
//       @Override
//        protected String doInBackground(Object... objects) {
//            ResponseBody callLogCallerInfo = null;
//            try {
//                callLogCallerInfo = restClient.get("/restapi/v1.0/account/~/extension/~/call-log/" + call_id);
//
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            assertNotNull(callLogCallerInfo);
//            String result = null;
//            try {
//                result = callLogCallerInfo.string();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//            System.out.print("Get_Call_Detail" + result);
//            final Call_Log call_log = new Call_Log();
//
//            try {
//                JSONObject jsonObject = new JSONObject(result);
//                call_log.setUri(jsonObject.getString("uri"));
//                call_log.setId(jsonObject.getString("id"));
//                call_log.setSessionId(jsonObject.getString("sessionId"));
//                if (jsonObject.has("to")) {
//                    To_Call_Recode toCall = new To_Call_Recode();
//                    String jsonObjectto = jsonObject.getString("to");
//                    toCall = ObjectCaster.jSONcast(To_Call_Recode.class, jsonObjectto);
//
//                    call_log.setTo_Call_Recode(toCall);
//                }
//                if (jsonObject.has("from")) {
//                    From_Call_Recod_Detail fromCall = new From_Call_Recod_Detail();
//                    String jsonObjectfrom = jsonObject.getString("from");
//                    fromCall = ObjectCaster.jSONcast(From_Call_Recod_Detail.class, jsonObjectfrom);
//                    call_log.setFrom_Call_Recod_Detail(fromCall);
//                }
//                call_log.setSessionId(jsonObject.getString("type"));
//                call_log.setSessionId(jsonObject.getString("direction"));
//                call_log.setSessionId(jsonObject.getString("action"));
//                call_log.setSessionId(jsonObject.getString("result"));
//                call_log.setSessionId(jsonObject.getString("startTime"));
//                call_log.setSessionId(jsonObject.getString("duration"));
//                if (jsonObject.has("recording")) {
//                    Recording recording = new Recording();
//                    String jsonObjectrecording = jsonObject.getString("recording");
//                    recording = ObjectCaster.jSONcast(Recording.class, jsonObjectrecording);
//                    call_log.setRecording(recording);
//                }
//            call_logList.add(call_log);
//            } catch (JSONException e) {
//                e.printStackTrace();
//            }
//            if(pho.equalsIgnoreCase(call_log.getFrom_Call_Recod_Detail().getPhoneNumber())){
//            mDatabase = FirebaseDatabase.getInstance().getReference("call_log");
//            mDatabase.orderByChild("id").equalTo(call_log.getId())
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            if (dataSnapshot.exists()) {
//                                System.out.print("exit");
//                            } else {
//                                System.out.print("no exit");
//                                String id = mDatabase.push().getKey();
//                                mDatabase.child(id).setValue(call_log);
//                            }
//                        }
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//
//                        }
//                    });
//        } else if(phonCodeWithOutPlus.equalsIgnoreCase(call_log.getFrom_Call_Recod_Detail().getPhoneNumber())) {
//            mDatabase = FirebaseDatabase.getInstance().getReference("call_log");
//            mDatabase.orderByChild("id").equalTo(call_log.getId())
//                    .addListenerForSingleValueEvent(new ValueEventListener() {
//                        @Override
//                        public void onDataChange(DataSnapshot dataSnapshot) {
//                            if (dataSnapshot.exists()) {
//                                System.out.print("exit");
//                            } else {
//                                System.out.print("no exit");
//                                String id = mDatabase.push().getKey();
//                                mDatabase.child(id).setValue(call_log);
//                            }
//                        }
//                        @Override
//                        public void onCancelled(DatabaseError databaseError) {
//                        }
//                    });
//        } else if(phonCodeWithPlus.equalsIgnoreCase(call_log.getFrom_Call_Recod_Detail().getPhoneNumber())) {
//
//                mDatabase = FirebaseDatabase.getInstance().getReference("call_log");
//                mDatabase.orderByChild("id").equalTo(call_log.getId())
//                        .addListenerForSingleValueEvent(new ValueEventListener() {
//                            @Override
//                            public void onDataChange(DataSnapshot dataSnapshot) {
//                                if (dataSnapshot.exists()) {
//                                    System.out.print("exit");
//                                } else {
//                                    System.out.print("no exit");
//                                    String id = mDatabase.push().getKey();
//                                    mDatabase.child(id).setValue(call_log);
//                                }
//                            }
//
//                            @Override
//                            public void onCancelled(DatabaseError databaseError) {
//                            }
//                        });
//            }
//            System.out.print(call_log);
//
//            return null;
//        }
//    }


//    public class Send_Call extends AsyncTask<Void, Void, Void> {
//        @Override
//        protected Void doInBackground(Void... voids) {
//            RingOutResource ringOutResource = new RingOutResource();
//            ringOutResource.from = new RingOutFromInfo().phoneNumber(Config.username);
//            ringOutResource.to = new RingOutPhoneNumberInfo().phoneNumber(Config.receiver);
//            ringOutResource.playPrompt = new Boolean(true);
//            ringOutResource.country = new RingOutCountry().id("91");
//            RingOutInfo ringOutInfo;
//            try {
//                ringOutInfo = restClient.post("/restapi/v1.0/account/~/extension/~/ring-out", ringOutResource, RingOutInfo.class);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }

//    public class Send_SMS extends AsyncTask<Object, Object, String> {
//
//        @Override
//        protected String doInBackground(Object... objects) {
//            CreateSMSMessage postParameters = new CreateSMSMessage();
//            postParameters.from = new MessageStoreCallerInfoRequest().phoneNumber(Config.username);
//            postParameters.to = new MessageStoreCallerInfoRequest[]{new MessageStoreCallerInfoRequest().phoneNumber(Config.receiver)};
//            postParameters.text = "hello world";
//            MessageInfo messageInfo = null;
//            try {
//                messageInfo = restClient.post("/restapi/v1.0/account/~/extension/~/sms", postParameters, MessageInfo.class);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            return null;
//        }
//    }

//    public class Send_READ_CALL_LOG extends AsyncTask<Object, Object, String> {
//        @Override
//        protected String doInBackground(Object... objects) {
//            CallLogCallerInfo callLogCallerInfo = null;
//            try {
//                callLogCallerInfo = restClient.get("/restapi/v1.0/account/~/extension/~/call-log", CallLogCallerInfo.class);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            assertNotNull(callLogCallerInfo);
//            return null;
//        }
//    }

//    public class Web_Hook extends AsyncTask<Object, Object, String> {
//        @Override
//        protected String doInBackground(Object... objects) {
//            CallLogCallerInfo callLogCallerInfo = null;
//            try {
//                callLogCallerInfo = restClient.get("/restapi/v1.0/account/~/extension/~/answering-rule/", WebUrisResource.class);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            assertNotNull(callLogCallerInfo);
//            return null;
//        }
//    }

    public class Get_Call_Detail extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... objects) {
            ResponseBody responseBody = null;
            try {
                responseBody = restClient.get("/restapi/v1.0/account/~/extension/~/call-log?extensionNumber=101&direction=Inbound&phoneNumber=" + pho + "&dateTo=2018-02-7&dateFrom=2018-01-1");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            }
            assertNotNull(responseBody);
            try {
                String result = responseBody.string();
                System.out.print("Get_Call_Detail" + result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String uri = jsonObject.getString("uri");
                    JSONArray records = jsonObject.getJSONArray("records");
                    String paging = jsonObject.getString("paging");
                    String navigation = jsonObject.getString("navigation");
                    for (int i = 0; i < records.length(); i++) {
                        final RecordCall recordCall = new RecordCall();
                        ToCall toCall = new ToCall();
                        FromCall fromCall = new FromCall();
                        JSONObject jsonObject1 = records.getJSONObject(i);
                        recordCall.setUri(jsonObject1.getString("uri"));
                        recordCall.setId(jsonObject1.getString("id"));
                        recordCall.setSessionId(jsonObject1.getString("sessionId"));
                        recordCall.setStartTime(jsonObject1.getString("startTime"));
                        recordCall.setDuration(Integer.valueOf(jsonObject1.getString("duration")));
                        recordCall.setType(jsonObject1.getString("type"));
                        recordCall.setDirection(jsonObject1.getString("direction"));
                        recordCall.setAction(jsonObject1.getString("action"));
                        recordCall.setResult(jsonObject1.getString("result"));
                        if (jsonObject1.has("to")) {
                            String jsonObjectto = jsonObject1.getString("to");
                            toCall = ObjectCaster.jSONcast(ToCall.class, jsonObjectto);
                            recordCall.setToCall(toCall);
                        }
                        if (jsonObject1.has("from")) {
                            String jsonObjectfrom = jsonObject1.getString("from");
                            fromCall = ObjectCaster.jSONcast(FromCall.class, jsonObjectfrom);
                            recordCall.setFromCall(fromCall);
                        }
                        recordListCall.add(recordCall);
                        System.out.print(recordListCall);
                        if (pho.equalsIgnoreCase(recordCall.getFromCall().getPhoneNumber())) {
                            mDatabase = FirebaseDatabase.getInstance().getReference("recordCall");
                            mDatabase.orderByChild("id").equalTo(recordCall.getId())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                System.out.print("exit");
                                            } else {
                                                System.out.print("no exit");
                                                String id = mDatabase.push().getKey();
                                                mDatabase.child(id).setValue(recordCall);
                                            }
                                        }
                                       @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                        } else if (phonCodeWithOutPlus.equalsIgnoreCase(recordCall.getFromCall().getPhoneNumber())) {
                            recordListCall.add(recordCall);
                            System.out.print(recordListCall);
                            mDatabase = FirebaseDatabase.getInstance().getReference("recordCall");
                            mDatabase.orderByChild("id").equalTo(recordCall.getId())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                System.out.print("exit");
                                            } else {
                                                System.out.print("no exit");
                                                String id = mDatabase.push().getKey();
                                                mDatabase.child(id).setValue(recordCall);
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                        } else if (phonCodeWithPlus.equalsIgnoreCase(recordCall.getFromCall().getPhoneNumber())) {
                            recordListCall.add(recordCall);
                            System.out.print(recordListCall);
                            mDatabase = FirebaseDatabase.getInstance().getReference("recordCall");
                            mDatabase.orderByChild("id").equalTo(recordCall.getId())
                                    .addListenerForSingleValueEvent(new ValueEventListener() {
                                        @Override
                                        public void onDataChange(DataSnapshot dataSnapshot) {
                                            if (dataSnapshot.exists()) {
                                                System.out.print("exit");
                                            } else {
                                                System.out.print("no exit");
                                                String id = mDatabase.push().getKey();
                                                mDatabase.child(id).setValue(recordCall);
                                            }
                                        }
                                        @Override
                                        public void onCancelled(DatabaseError databaseError) {
                                        }
                                    });
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
            }
            Intent intent = new Intent(MainActivity.this, HistoryCall.class);
            startActivity(intent);
            finish();
            return null;
        }
    }

    public class Get_Messages extends AsyncTask<Object, Object, String> {
        @Override
        protected String doInBackground(Object... objects) {
            ResponseBody responseBody = null;
            try {
                responseBody = restClient.get("restapi/v1.0/account/~/extension/~/message-store?extensionNumber=101&dateFrom=2018-01-1&phoneNumber=" + pho + "&dateTo=2018-01-23");
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            }
            assertNotNull(responseBody);
            try {
                String result = responseBody.string();
                System.out.print("mnzcbjh" + result);
                try {
                    JSONObject jsonObject = new JSONObject(result);
                    String uri = jsonObject.getString("uri");
                    JSONArray records = jsonObject.getJSONArray("records");
                    String paging = jsonObject.getString("paging");
                    String navigation = jsonObject.getString("navigation");
                    for (int i = 0; i < records.length(); i++) {
                        final Record record = new Record();
                        To to = new To();
                        From from = new From();
                        Attachment attachment = new Attachment();
                        Conversation conversation = new Conversation();
                        JSONObject jsonObject1 = records.getJSONObject(i);
                        record.setUri(jsonObject1.getString("uri"));
                        record.setId((int) Long.parseLong(jsonObject1.getString("id")));
                        record.setType(jsonObject1.getString("type"));
                        record.setCreationTime(jsonObject1.getString("creationTime"));
                        record.setReadStatus(jsonObject1.getString("readStatus"));
                        record.setPriority(jsonObject1.getString("priority"));
                        record.setDirection(jsonObject1.getString("direction"));
                        record.setAvailability(jsonObject1.getString("availability"));
                        record.setMessageStatus(jsonObject1.getString("messageStatus"));
                        record.setLastModifiedTime(jsonObject1.getString("lastModifiedTime"));
                        JSONArray jsonArrayto = jsonObject1.getJSONArray("to");
                        for (int j = 0; j < jsonArrayto.length(); j++) {
                            JSONObject jsonObjectto = jsonArrayto.getJSONObject(j);
                            to.setPhoneNumber(jsonObjectto.getString("phoneNumber"));
                            record.setTo(to);
                        }
                        JSONArray jsonArrayattachments = jsonObject1.getJSONArray("attachments");
                        for (int j = 0; j < jsonArrayattachments.length(); j++) {
                            JSONObject jsonObjectto = jsonArrayattachments.getJSONObject(j);
                            attachment.setId(jsonObjectto.getInt("id"));
                            attachment.setType(jsonObjectto.getString("type"));
                            attachment.setContentType(jsonObjectto.getString("contentType"));
                            record.setAttachments(attachment);
                        }
                        if (jsonObject1.has("subject")) {
                            record.setSubject(jsonObject1.getString("subject"));
                        }
                        if (jsonObject1.has("conversationId")) {
                            record.setConversationId(jsonObject1.getInt("conversationId"));
                        }
                        if (jsonObject1.has("conversation")) {
                            String jsonObjectconversation = jsonObject1.getString("conversation");
                            conversation = ObjectCaster.jSONcast(Conversation.class, jsonObjectconversation);
                            record.setConversation(conversation);
                        }
                        if (jsonObject1.has("from")) {
                            String jsonObjectfrom = jsonObject1.getString("from");
                            from = ObjectCaster.jSONcast(From.class, jsonObjectfrom);
                            record.setFrom(from);
                        }
                        if (jsonObject1.has("faxResolution")) {
                            record.setFaxResolution(jsonObject1.getString("faxResolution"));
                        }
                        if (jsonObject1.has("faxPageCount")) {
                            record.setFaxPageCount(jsonObject1.getString("faxPageCount"));
                        }
                        if (jsonObject1.has("coverIndex")) {
                            record.setCoverIndex(jsonObject1.getString("coverIndex"));
                        }
                        recordList.add(record);
                        System.out.print(recordList);
                        mDatabase = FirebaseDatabase.getInstance().getReference("record");
                        mDatabase.orderByChild("id").equalTo(record.getId())
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.exists()) {
                                            System.out.print("exit");
                                        } else {
                                            System.out.print("no exit");
                                            String id = mDatabase.push().getKey();
                                            mDatabase.child(id).setValue(record);
                                        }
                                    }
                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {
                                    }
                                });
                    }
//                    System.out.print(example);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
            }
//            mDatabase = FirebaseDatabase.getInstance().getReference("recordList");
//            String id = mDatabase.push().getKey();
//            mDatabase.child(id).setValue(recordList);
            Intent intent = new Intent(MainActivity.this, HistoryMessage.class);
            startActivity(intent);
            return null;
        }
    }

//    public class Get_MassegeStore extends AsyncTask<Object, Object, String> {
//        @Override
//        protected String doInBackground(Object... objects) {
//            Message_Histore message_histore = new Message_Histore();
//            message_histore.setConversationId(null);
//            message_histore.setDateFrom("2018-01-1");
//            message_histore.setDateTo("2018-01-23");
//            message_histore.setDistinctConversations(true);
//            message_histore.setPhoneNumber("14153912707");
//            ResponseBody callLogCallerInfo = null;
//            try {
//                callLogCallerInfo = restClient.get("/restapi/v1.0/account/~/extension/~/message-store?availability=" + message_histore.getAvailability() + "&direction=" + message_histore.getDirection() + "&messageType=" + message_histore.getMessageType() + "&readStatus=" + message_histore.getReadStatus() + "&phoneNumber=" + message_histore.getPhoneNumber());
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            assertNotNull(callLogCallerInfo);
//            return null;
//        }
//    }


//    public class Send_FAX extends AsyncTask<Object, Object, String> {
//        @Override
//        protected String doInBackground(Object... objects) {
//            RequestBody requestBody = null;
//            try {
//                requestBody = new MultipartBody.Builder().setType(MultipartBody.MIXED)
//                        .addPart(RequestBody.create(MediaType.parse("application/json"),
//                                "{ \"to\": [{ \"phoneNumber\": " + Config.receiver + " }]}"))
//                        .addFormDataPart("attachment", "test.txt", RequestBody.create(MediaType.parse("text/plain"), "Hello world"))
////                            .addFormDataPart("attachment", "test.png", RequestBody.create(MediaType.parse("image/png"),file.getPath().getBytes()))
//                        .build();
//
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//            try {
//                restClient.post("/restapi/v1.0/account/~/extension/~/fax", requestBody);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (RestException e) {
//                e.printStackTrace();
//            }
//            assertNotNull(restClient);
//            return null;
//        }
//    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public class Send_Token extends AsyncTask<Object, Object, String> {
        @Override
        protected String doInBackground(Object... objects) {
            try {
                restClient.authorize(Config.username, Config.extension, Config.password);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            }
            return null;
        }
    }
}
