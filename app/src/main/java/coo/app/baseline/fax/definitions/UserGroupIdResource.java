package coo.app.baseline.fax.definitions;

public class UserGroupIdResource
{
    //
    public String uri;
    public UserGroupIdResource uri(String uri) {
        this.uri = uri;
        return this;
    }
    //
    public String id;
    public UserGroupIdResource id(String id) {
        this.id = id;
        return this;
    }
}
