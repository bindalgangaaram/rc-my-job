package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/25/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Record {

    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("to")
    @Expose
    private To to = null;
    @SerializedName("from")
    @Expose
    private From from;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("creationTime")
    @Expose
    private String creationTime;
    @SerializedName("readStatus")
    @Expose
    private String readStatus;
    @SerializedName("priority")
    @Expose
    private String priority;
    @SerializedName("attachments")
    @Expose
    private Attachment attachments = null;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("availability")
    @Expose
    private String availability;
    @SerializedName("subject")
    @Expose
    private String subject;
    @SerializedName("messageStatus")
    @Expose
    private String messageStatus;
    @SerializedName("conversationId")
    @Expose
    private Integer conversationId;
    @SerializedName("conversation")
    @Expose
    private Conversation conversation;
    @SerializedName("lastModifiedTime")
    @Expose
    private String lastModifiedTime;


    @SerializedName("faxResolution")
    @Expose
    private String faxResolution;

    @SerializedName("faxPageCount")
    @Expose
    private String faxPageCount;

    @SerializedName("coverIndex")
    @Expose
    private String coverIndex;


    public String getFaxResolution() {
        return faxResolution;
    }

    public void setFaxResolution(String faxResolution) {
        this.faxResolution = faxResolution;
    }

    public String getFaxPageCount() {
        return faxPageCount;
    }

    public void setFaxPageCount(String faxPageCount) {
        this.faxPageCount = faxPageCount;
    }

    public String getCoverIndex() {
        return coverIndex;
    }

    public void setCoverIndex(String coverIndex) {
        this.coverIndex = coverIndex;
    }


    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public To getTo() {
        return to;
    }

    public void setTo(To to) {
        this.to = to;
    }

    public From getFrom() {
        return from;
    }

    public void setFrom(From from) {
        this.from = from;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }

    public String getReadStatus() {
        return readStatus;
    }

    public void setReadStatus(String readStatus) {
        this.readStatus = readStatus;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }

    public Attachment getAttachments() {
        return attachments;
    }

    public void setAttachments(Attachment attachments) {
        this.attachments = attachments;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessageStatus() {
        return messageStatus;
    }

    public void setMessageStatus(String messageStatus) {
        this.messageStatus = messageStatus;
    }

    public Integer getConversationId() {
        return conversationId;
    }

    public void setConversationId(Integer conversationId) {
        this.conversationId = conversationId;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public String getLastModifiedTime() {
        return lastModifiedTime;
    }

    public void setLastModifiedTime(String lastModifiedTime) {
        this.lastModifiedTime = lastModifiedTime;
    }

}

