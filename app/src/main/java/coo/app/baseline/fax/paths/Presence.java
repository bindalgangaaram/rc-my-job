package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Line;

public class Presence extends Path {
    public Presence (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "presence", id);
    }
    public com.ringcentral.paths.Line line(String id)
    {
        return new com.ringcentral.paths.Line(restClient, pathSegment, id);
    }
    public com.ringcentral.paths.Line line()
    {
        return new Line(restClient, pathSegment, null);
    }
    public com.ringcentral.paths.Permission permission(String id)
    {
        return new com.ringcentral.paths.Permission(restClient, pathSegment, id);
    }
    public com.ringcentral.paths.Permission permission()
    {
        return new com.ringcentral.paths.Permission(restClient, pathSegment, null);
    }
}
