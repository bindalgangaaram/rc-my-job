package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Account;
import com.ringcentral.paths.ClientInfo;
import com.ringcentral.paths.NumberParser;
import com.ringcentral.paths.NumberPool;
import com.ringcentral.paths.Scim;

public class Restapi extends Path {
    public Restapi (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "restapi", id);
    }
    public Status status()
    {
        return new Status(restClient, pathSegment, null);
    }
    public com.ringcentral.paths.Account account(String id)
    {
        return new com.ringcentral.paths.Account(restClient, pathSegment, id);
    }
    public com.ringcentral.paths.Account account()
    {
        return new Account(restClient, pathSegment, "~");
    }
    public Dictionary dictionary()
    {
        return new Dictionary(restClient, pathSegment, null);
    }
    public Glip glip()
    {
        return new Glip(restClient, pathSegment, null);
    }
    public Subscription subscription(String id)
    {
        return new Subscription(restClient, pathSegment, id);
    }
    public Subscription subscription()
    {
        return new Subscription(restClient, pathSegment, null);
    }
    public ClientInfo clientInfo()
    {
        return new ClientInfo(restClient, pathSegment, null);
    }
    public NumberParser numberParser()
    {
        return new NumberParser(restClient, pathSegment, null);
    }
    public NumberPool numberPool()
    {
        return new NumberPool(restClient, pathSegment, null);
    }
    public Scim scim()
    {
        return new Scim(restClient, pathSegment, null);
    }
}
