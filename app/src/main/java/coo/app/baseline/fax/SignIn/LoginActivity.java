package coo.app.baseline.fax.SignIn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hbb20.CountryCodePicker;

import coo.app.baseline.fax.MainActivity;
import coo.app.baseline.fax.R;
import coo.app.baseline.fax.SessionManagement;


public class LoginActivity extends AppCompatActivity {
    //     implements GoogleApiClient.OnConnectionFailedListener
//    private static final String TAG = LoginActivity.class.getSimpleName();
//    private static final int RC_SIGN_IN = 007;
//    TextView tet_registation;
//    private GoogleApiClient mGoogleApiClient;
//    private ProgressDialog mProgressDialog;
//    private SignInButton btn_sign_in_with_gmail;
//    CallbackManager callbackManager;
//    private DatabaseReference mDatabase;
//    private LoginButton login_button_with_facebook;
//    private ProgressDialog progressDialog;
//    private FirebaseAuth firebaseAuth;
//    private Button sign_in_with_phone;
//    Button sign_in_with_email;
//    String resr = null;
    EditText phone;
    Button login;
    CountryCodePicker ccp_loadFullNumber;
    String codeWithPlus,codeWithOutPlus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        phone = (EditText) findViewById(R.id.phone);
        login = (Button) findViewById(R.id.login);
        ccp_loadFullNumber = (CountryCodePicker) findViewById(R.id.ccp_loadFullNumber);

        login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                codeWithPlus = ccp_loadFullNumber.getSelectedCountryCodeWithPlus();
                codeWithOutPlus = ccp_loadFullNumber.getSelectedCountryCode();
                String str_phopne =  phone.getText().toString();

                if (str_phopne.isEmpty()) {
                    phone.setError("Please enter the phone number");
                } else {
                    SessionManagement sessionManagement = new SessionManagement(getApplicationContext());
                    sessionManagement.createLoginSession(str_phopne);
                    Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                    intent.putExtra("phonenumber", str_phopne);
                    intent.putExtra("codeWithPlus",codeWithPlus);
                    intent.putExtra("codeWithOutPlus",codeWithOutPlus);
                    startActivity(intent);
                }
            }
        });

    }
}
      /*  firebaseAuth = FirebaseAuth.getInstance();

        btn_sign_in_with_gmail = (SignInButton) findViewById(R.id.btn_sign_in_with_gmail);
        sign_in_with_email = (Button) findViewById(R.id.sign_in_with_email);
        sign_in_with_phone = (Button) findViewById(R.id.sign_in_with_phone);

        sign_in_with_email.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });

        progressDialog = new ProgressDialog(LoginActivity.this);
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(LoginActivity.this)
                .enableAutoManage(LoginActivity.this, LoginActivity.this)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        // Customizing G+ button
        btn_sign_in_with_gmail.setSize(SignInButton.SIZE_STANDARD);
        btn_sign_in_with_gmail.setScopes(gso.getScopeArray());

        callbackManager = CallbackManager.Factory.create();
        LoginButton login_button_with_facebook = (LoginButton) findViewById(R.id.login_button_with_facebook);
        login_button_with_facebook.setReadPermissions("email");

        login_button_with_facebook.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                getUserDetails(loginResult);
            }

            @Override
            public void onCancel() {
                // App code
            }

            @Override
            public void onError(FacebookException exception) {
                // App code
            }
        });


        btn_sign_in_with_gmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                resr = "dsjk";
                signIn();
            }
        });
        sign_in_with_phone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(LoginActivity.this,Sign_in_with_phone.class);
                startActivity(intent);
            }
        });

    }

    private void signIn() {
        Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Log.d(TAG, "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();
            Log.e(TAG, "display name: " + acct.getDisplayName());
            String personName = acct.getDisplayName();
            String email = acct.getEmail();

            if (resr == null) {

            } else {
                resr = null;
                SessionManagement sessionManagement = new SessionManagement(getApplicationContext());
                sessionManagement.createLoginSession(personName, email);

                 Intent intent = new Intent(LoginActivity.this, MainActivity.class);
                startActivity(intent);
            }
        } else {

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    protected void getUserDetails(LoginResult loginResult) {
        GraphRequest data_request = GraphRequest.newMeRequest(
                loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                    @Override
                    public void onCompleted(
                            JSONObject json_object,
                            GraphResponse response) {
                        Intent intent = new Intent(LoginActivity.this, MainActivity.class);
//                        intent.putExtra("userProfile", json_object.toString());
                        startActivity(intent);
                    }

                });
        Bundle permission_param = new Bundle();
        permission_param.putString("fields", "id,name,email,picture.width(120).height(120)");
        data_request.setParameters(permission_param);
        data_request.executeAsync();
    }

    protected void onResume() {
        super.onResume();
        // Logs 'install' and 'app activate' App Events.
        AppEventsLogger.activateApp(this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        // Logs 'app deactivate' App Event.
        AppEventsLogger.deactivateApp(this);
    }


//    @Override
//    public void onClick(View view) {
//        signIn();
//    }
//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//
//        // Result returned fromPOJO launching the Intent fromPOJO GoogleSignInApi.getSignInIntent(...);
//        if (requestCode == RC_SIGN_IN) {
//            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
//            handleSignInResult(result);
//        }
//    }

    @Override
    public void onStart() {
        super.onStart();

        OptionalPendingResult<GoogleSignInResult> opr = Auth.GoogleSignInApi.silentSignIn(mGoogleApiClient);
        if (opr.isDone()) {
            // If the user's cached credentials are valid, the OptionalPendingResult will be "done"
            // and the GoogleSignInResult will be available instantly.
            Log.d(TAG, "Got cached sign-in");
            GoogleSignInResult result = opr.get();
            handleSignInResult(result);
        } else {
            // If the user has not previously signed in on this device or the sign-in has expired,
            // this asynchronous branch will attempt to sign in the user silently.  Cross-device
            // single sign-on will occur in this branch.
            showProgressDialog();
            opr.setResultCallback(new ResultCallback<GoogleSignInResult>() {
                @Override
                public void onResult(GoogleSignInResult googleSignInResult) {
                    hideProgressDialog();
                    handleSignInResult(googleSignInResult);
                }
            });
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Log.d(TAG, "onConnectionFailed:" + connectionResult);
    }

    private void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }
        mProgressDialog.show();
    }

    private void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.hide();
        }
    }



}*/