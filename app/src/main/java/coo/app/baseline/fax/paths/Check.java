package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;

public class Check extends Path {
    public Check (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "check", id);
    }
    public static class GetParameters
    {
        //
        public String permissionId;
        public GetParameters permissionId(String permissionId) {
            this.permissionId = permissionId;
            return this;
        }
        //
        public String targetExtensionId;
        public GetParameters targetExtensionId(String targetExtensionId) {
            this.targetExtensionId = targetExtensionId;
            return this;
        }
    }
}
