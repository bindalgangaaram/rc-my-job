package coo.app.baseline.fax.definitions;

public class ServiceInfoRequest
{
    // Limits which are effective for an account
    public AccountLimits limits;
    public ServiceInfoRequest limits(AccountLimits limits) {
        this.limits = limits;
        return this;
    }
}
