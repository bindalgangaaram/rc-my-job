package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/25/2018.
 */

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example {

    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("records")
    @Expose
    private List<Record> records = null;
    @SerializedName("paging")
    @Expose
    private Paging paging;
    @SerializedName("navigation")
    @Expose
    private Navigation navigation;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public List<Record> getRecords() {
        return records;
    }

    public void setRecords(List<Record> records) {
        this.records = records;
    }

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Navigation getNavigation() {
        return navigation;
    }

    public void setNavigation(Navigation navigation) {
        this.navigation = navigation;
    }

}