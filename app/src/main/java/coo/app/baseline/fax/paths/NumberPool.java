package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Lookup;
import com.ringcentral.paths.Reserve;

public class NumberPool extends Path {
    public NumberPool (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "number-pool", id);
    }
    public Lookup lookup()
    {
        return new Lookup(restClient, pathSegment, null);
    }
    public Reserve reserve()
    {
        return new Reserve(restClient, pathSegment, null);
    }
}
