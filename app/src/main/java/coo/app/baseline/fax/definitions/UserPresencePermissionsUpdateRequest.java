package coo.app.baseline.fax.definitions;


public class UserPresencePermissionsUpdateRequest
{
    //
    public PresencePermissionsExtensionInfoRequest[] extensions;
    public UserPresencePermissionsUpdateRequest extensions(PresencePermissionsExtensionInfoRequest[] extensions) {
        this.extensions = extensions;
        return this;
    }
}
