package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Settings;

public class Reporting extends Path {
    public Reporting (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "reporting", id);
    }
    public com.ringcentral.paths.Settings settings()
    {
        return new Settings(restClient, pathSegment, null);
    }
}
