package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Content;

public class CustomData extends Path {
    public CustomData (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "custom-data", id);
    }
    public com.ringcentral.paths.Content content(String id)
    {
        return new com.ringcentral.paths.Content(restClient, pathSegment, id);
    }
    public com.ringcentral.paths.Content content()
    {
        return new Content(restClient, pathSegment, null);
    }
}
