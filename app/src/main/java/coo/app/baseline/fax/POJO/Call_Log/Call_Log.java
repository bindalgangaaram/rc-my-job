package coo.app.baseline.fax.POJO.Call_Log;

/**
 * Created by Android on 2/5/2018.
 */

import java.io.Serializable;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import coo.app.baseline.fax.POJO.Call_Log.From_Call_Recod_Detail;
import coo.app.baseline.fax.POJO.Call_Log.To_Call_Recode;

public class Call_Log implements Serializable
{

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("sessionId")
    @Expose
    private String sessionId;
    @SerializedName("from")
    @Expose
    private From_Call_Recod_Detail from;
    @SerializedName("to")
    @Expose
    private To_Call_Recode to;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("recording")
    @Expose
    private Recording recording;
    private final static long serialVersionUID = -2015604161607182885L;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public From_Call_Recod_Detail getFrom_Call_Recod_Detail() {
        return from;
    }

    public void setFrom_Call_Recod_Detail(From_Call_Recod_Detail from) {
        this.from = from;
    }

    public To_Call_Recode getTo_Call_Recode() {
        return to;
    }

    public void setTo_Call_Recode(To_Call_Recode to) {
        this.to = to;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public Recording getRecording() {
        return recording;
    }

    public void setRecording(Recording recording) {
        this.recording = recording;
    }

}