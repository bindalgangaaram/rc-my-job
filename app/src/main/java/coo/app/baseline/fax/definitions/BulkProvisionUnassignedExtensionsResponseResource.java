package coo.app.baseline.fax.definitions;


public class BulkProvisionUnassignedExtensionsResponseResource
{
    //
    public ExtensionAssignmentResult[] items;
    public BulkProvisionUnassignedExtensionsResponseResource items(ExtensionAssignmentResult[] items) {
        this.items = items;
        return this;
    }
}
