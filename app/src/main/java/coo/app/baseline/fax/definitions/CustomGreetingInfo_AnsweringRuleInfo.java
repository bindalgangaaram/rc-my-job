package coo.app.baseline.fax.definitions;

public class CustomGreetingInfo_AnsweringRuleInfo {
    // Internal identifier of an answering rule
    public String id;

    public CustomGreetingInfo_AnsweringRuleInfo id(String id) {
        this.id = id;
        return this;
    }
}
