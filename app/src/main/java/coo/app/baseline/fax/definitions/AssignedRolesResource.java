package coo.app.baseline.fax.definitions;
import com.ringcentral.definitions.AssignedRoleResource;

public class AssignedRolesResource
{
    //
    public String uri;
    public  AssignedRolesResource uri(String uri) {
        this.uri = uri;
        return this;
    }
    //
    public AssignedRoleResource[] records;
    public  AssignedRolesResource records(AssignedRoleResource[] records) {
        this.records = records;
        return this;
    }
}
