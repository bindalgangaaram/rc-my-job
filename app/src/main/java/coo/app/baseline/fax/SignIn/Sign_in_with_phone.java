package coo.app.baseline.fax.SignIn;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.hbb20.CountryCodePicker;
import com.ringcentral.RestException;

import java.io.IOException;
import java.util.Random;

import coo.app.baseline.fax.Config;
import coo.app.baseline.fax.R;
import coo.app.baseline.fax.RestClient;
import coo.app.baseline.fax.definitions.CreateSMSMessage;
import coo.app.baseline.fax.definitions.MessageInfo;
import coo.app.baseline.fax.definitions.MessageStoreCallerInfoRequest;

public class Sign_in_with_phone extends AppCompatActivity {
    EditText phone_number;
    Button send_otp;
    CountryCodePicker ccp_loadFullNumber;
    String code;
    int otp_number;
    RestClient restClient;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in_with_phone);
        send_otp = (Button) findViewById(R.id.send_otp);
        phone_number = (EditText) findViewById(R.id.phone_number);
        restClient = new RestClient(Config.appKey, Config.appSecret, Config.server);

//        ccp_loadFullNumber.setOnCountryChangeListener(new CountryCodePicker.OnCountryChangeListener() {
//            @Override
//            public void onCountrySelected() {
//                code = ccp_loadFullNumber.getSelectedCountryCode().toString();
//                System.out.print(code);
//                Random rnd = new Random();
//                otp_number = 100000 + rnd.nextInt(900000);
//                Send_Token send_token = new Send_Token();
//                send_token.execute();
//
//            }
//        });
//        send_otp.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Send_SMS send_sms = new Send_SMS();
//                send_sms.execute();
//            }
//        });


   }



    public class Send_SMS extends AsyncTask<Object, Object, String> {

        @Override
        protected String doInBackground(Object... objects) {
            CreateSMSMessage postParameters = new CreateSMSMessage();
            postParameters.from = new MessageStoreCallerInfoRequest().phoneNumber("14153912707");
            postParameters.to = new MessageStoreCallerInfoRequest[]{new MessageStoreCallerInfoRequest().phoneNumber("918077981043")};
            postParameters.text = String.valueOf(otp_number);
            MessageInfo messageInfo;
            try {
                messageInfo = restClient.post("/restapi/v1.0/account/~/extension/~/sms", postParameters, MessageInfo.class);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public class Send_Token extends AsyncTask<Object, Object, String> {
        @Override
        protected String doInBackground(Object... objects) {
            try {
                restClient.authorize(Config.username, Config.extension, Config.password);
            } catch (IOException e) {
                e.printStackTrace();
            } catch (RestException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

}
