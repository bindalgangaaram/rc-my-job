package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/25/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class From {

    @SerializedName("phoneNumber")
    @Expose
    private String phoneNumber;
    @SerializedName("location")
    @Expose
    private String location;

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

}