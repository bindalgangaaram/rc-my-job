package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/30/2018.
 */


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RecordCall {

    @SerializedName("uri")
    @Expose
    private String uri;
    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("sessionId")
    @Expose
    private String sessionId;
    @SerializedName("startTime")
    @Expose
    private String startTime;
    @SerializedName("duration")
    @Expose
    private Integer duration;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("direction")
    @Expose
    private String direction;
    @SerializedName("action")
    @Expose
    private String action;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("to")
    @Expose
    private ToCall to;
    @SerializedName("from")
    @Expose
    private FromCall from;

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public String getStartTime() {
        return startTime;
    }

    public void setStartTime(String startTime) {
        this.startTime = startTime;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public ToCall getToCall() {
        return to;
    }

    public void setToCall(ToCall to) {
        this.to = to;
    }

    public FromCall getFromCall() {
        return from;
    }

    public void setFromCall(FromCall from) {
        this.from = from;
    }

}