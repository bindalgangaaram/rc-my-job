package coo.app.baseline.fax.definitions;

public class OrganizationResource
{
    //
    public String id;
    public OrganizationResource id(String id) {
        this.id = id;
        return this;
    }
    //
    public String name;
    public OrganizationResource name(String name) {
        this.name = name;
        return this;
    }
}
