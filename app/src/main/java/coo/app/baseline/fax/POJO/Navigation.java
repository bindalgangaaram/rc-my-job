package coo.app.baseline.fax.POJO;

/**
 * Created by Android on 1/25/2018.
 */



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Navigation {

    @SerializedName("firstPage")
    @Expose
    private FirstPage firstPage;
    @SerializedName("lastPage")
    @Expose
    private LastPage lastPage;

    public FirstPage getFirstPage() {
        return firstPage;
    }

    public void setFirstPage(FirstPage firstPage) {
        this.firstPage = firstPage;
    }

    public LastPage getLastPage() {
        return lastPage;
    }

    public void setLastPage(LastPage lastPage) {
        this.lastPage = lastPage;
    }

}


