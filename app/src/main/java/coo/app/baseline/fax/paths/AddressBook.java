package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Contact;

public class AddressBook extends Path {
    public AddressBook (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "address-book", id);
    }
    public com.ringcentral.paths.Contact contact(String id)
    {
        return new com.ringcentral.paths.Contact(restClient, pathSegment, id);
    }
    public com.ringcentral.paths.Contact contact()
    {
        return new Contact(restClient, pathSegment, null);
    }
}
