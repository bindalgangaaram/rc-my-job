//package coo.app.baseline.fax.comm.Requests;
//
///**
// * Created by Android on 1/22/2018.
// */
//
////public class Message_Histore {
//
//    String availability [] = {"Alive","Deleted","Purged"} ;
//    String conversationId;
//    String dateFrom;
//    String dateTo;
//    String direction[] ={"Inbound","Outbound"};
//    Boolean distinctConversations;
//    long page;
//    long perPage;
//    String phoneNumber;
//    String messageType [] = {"Fax","SMS","VoiceMail"};
//    String readStatus [] = {"Red,Unread"};
//
//    public long getPage() {
//        return page;
//    }
//
//    public String[] getAvailability() {
//        return availability;
//    }
//
//    public void setAvailability(String[] availability) {
//        this.availability = availability;
//    }
//
//    public void setPage(long page) {
//
//        this.page = page;
//    }
//
//    public long getPerPage() {
//        return perPage;
//    }
//
//    public void setPerPage(long perPage) {
//        this.perPage = perPage;
//    }
//
//
//    public String[] getDirection() {
//        return direction;
//    }
//
//    public void setDirection(String[] direction) {
//        this.direction = direction;
//    }
//
//
//    public Boolean getDistinctConversations() {
//        return distinctConversations;
//    }
//
//    public void setDistinctConversations(Boolean distinctConversations) {
//        this.distinctConversations = distinctConversations;
//    }
//
//    public String[] getMessageType() {
//        return messageType;
//    }
//
//    public void setMessageType(String[] messageType) {
//        this.messageType = messageType;
//    }
//
//    public String[] getReadStatus() {
//        return readStatus;
//    }
//
//    public void setReadStatus(String[] readStatus) {
//        this.readStatus = readStatus;
//    }
//
//    public String getConversationId() {
//        return conversationId;
//    }
//
//    public void setConversationId(String conversationId) {
//        this.conversationId = conversationId;
//    }
//
//    public String getDateFrom() {
//        return dateFrom;
//    }
//
//    public void setDateFrom(String dateFrom) {
//        this.dateFrom = dateFrom;
//    }
//
//    public String getDateTo() {
//        return dateTo;
//    }
//
//    public void setDateTo(String dateTo) {
//        this.dateTo = dateTo;
//    }
//
//
//
//
//    public String getPhoneNumber() {
//        return phoneNumber;
//    }
//
//    public void setPhoneNumber(String phoneNumber) {
//        this.phoneNumber = phoneNumber;
//    }
//
//}
