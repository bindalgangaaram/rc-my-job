package coo.app.baseline.fax.definitions;

public class ExtensionTimezoneInfoRequest
{
    // internal Identifier for timezone
    public String id;
    public ExtensionTimezoneInfoRequest id(String id) {
        this.id = id;
        return this;
    }
}
