package coo.app.baseline.fax;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import coo.app.baseline.fax.POJO.Record;
import coo.app.baseline.fax.POJO.RecordCall;

public class HistoryMessage extends AppCompatActivity {
RecyclerView recyclerView;
DishAdapter dAdapter;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_message);
        recyclerView = (RecyclerView) findViewById(R.id.message_history);
         initViews();
    }

    private void initViews() {
        dAdapter = new DishAdapter(getApplicationContext(),MainActivity.recordList);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(dAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
    }
    class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder> {
        private List<Record> dishList;
        private Context context;
        public class DishViewHolder extends RecyclerView.ViewHolder {
            public TextView txt1_message, txt2_message;
             public Button btn_old_view;
            public DishViewHolder(View view) {
                super(view);
                txt1_message = (TextView) view.findViewById(R.id.txt1_message);
                txt2_message = (TextView) view.findViewById(R.id.txt2_message);
              }
        }
        public DishAdapter(Context context, List<Record> dishList) {
            this.context = context;
            this.dishList = dishList;
        }
        private DishViewHolder holder;
        @Override
        public DishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.message_hisstory, parent, false);
            return new DishViewHolder(itemView);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public void onBindViewHolder(DishViewHolder holder, int position) {
            final Record dish = dishList.get(position);
            holder.txt1_message.setText(dish.getTo().getPhoneNumber().toString());
            if(dish.getSubject() == null){
                System.out.print("null");
            }else {
                holder.txt2_message.setText(dish.getSubject().toString().toString());
            }

        }
        @Override
        public int getItemCount() {
            return dishList.size();
        }
    }
    @Override
    public void onBackPressed() {
         MainActivity.recordList.clear();
        super.onBackPressed();
    }

    }