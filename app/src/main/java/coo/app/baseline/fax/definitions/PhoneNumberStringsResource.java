package coo.app.baseline.fax.definitions;

public class PhoneNumberStringsResource
{
    //
    public String uri;
    public PhoneNumberStringsResource uri(String uri) {
        this.uri = uri;
        return this;
    }
    //
    public String[] originalStrings;
    public PhoneNumberStringsResource originalStrings(String[] originalStrings) {
        this.originalStrings = originalStrings;
        return this;
    }
}
