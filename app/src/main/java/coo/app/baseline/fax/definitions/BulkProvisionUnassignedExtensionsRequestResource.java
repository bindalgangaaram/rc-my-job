package coo.app.baseline.fax.definitions;


public class BulkProvisionUnassignedExtensionsRequestResource
{
    //
    public BulkProvisionExtensionResource[] items;
    public BulkProvisionUnassignedExtensionsRequestResource items(BulkProvisionExtensionResource[] items) {
        this.items = items;
        return this;
    }
}
