//package coo.app.baseline.fax;
//
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.support.v7.app.AppCompatActivity;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//
//import java.io.ByteArrayOutputStream;
//
//public class Fax extends AppCompatActivity {
//    Button attachButton,send_faxButton;
//    private static final int PICKFILE_RESULT_CODE = 1;
//    TextView textFile;
//    String FilePath,
//            server = RestClient.SANDBOX_SERVER;
//
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_fax);
//        attachButton = (Button)findViewById(R.id.attach);
//        send_faxButton = (Button)findViewById(R.id.send_fax);
//        send_faxButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            }
//        });
//
//        textFile = (TextView)findViewById(R.id.textfile);
//
//        attachButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
//                intent.setType("file/*");
//                startActivityForResult(intent,PICKFILE_RESULT_CODE);
//            }
//        });
//    }
//
//
//    @Override
//    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
//        // TODO Auto-generated method stub
//        switch(requestCode){
//            case PICKFILE_RESULT_CODE:
//                if(resultCode==RESULT_OK){
//                    FilePath = data.getData().getPath();
//                    textFile.setText(FilePath);
//                }
//                break;
//        }
//    }
//        // convert from bitmap to byte array
//        public byte[] getBytesFromBitmap(Bitmap bitmap) {
//            ByteArrayOutputStream stream = new ByteArrayOutputStream();
//            bitmap.compress(Bitmap.CompressFormat.JPEG, 70, stream);
//            return stream.toByteArray();
//        }
//}
