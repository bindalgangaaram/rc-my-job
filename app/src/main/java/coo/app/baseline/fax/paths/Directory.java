package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Contacts;
import com.ringcentral.paths.ExtensionNumbers;
import com.ringcentral.paths.Federation;
import com.ringcentral.paths.FederationConflicts;

public class Directory extends Path {
    public Directory (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "directory", id);
    }
    public com.ringcentral.paths.Contacts contacts(String id)
    {
        return new com.ringcentral.paths.Contacts(restClient, pathSegment, id);
    }
    public com.ringcentral.paths.Contacts contacts()
    {
        return new Contacts(restClient, pathSegment, null);
    }
    public Federation federation()
    {
        return new Federation(restClient, pathSegment, null);
    }
    public FederationConflicts federationConflicts()
    {
        return new FederationConflicts(restClient, pathSegment, null);
    }
    public ExtensionNumbers extensionNumbers()
    {
        return new ExtensionNumbers(restClient, pathSegment, null);
    }
}
