package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Default;

public class UserRole extends Path {
    public UserRole (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "user-role", id);
    }
    public Default _default()
    {
        return new Default(restClient, pathSegment, null);
    }
}
