package coo.app.baseline.fax.definitions;

public class ExtensionUserCredentials
{
    // Secret question of an extension user
    public SecretQuestionInfo secretQuestion;
    public ExtensionUserCredentials secretQuestion(SecretQuestionInfo secretQuestion) {
        this.secretQuestion = secretQuestion;
        return this;
    }
}
