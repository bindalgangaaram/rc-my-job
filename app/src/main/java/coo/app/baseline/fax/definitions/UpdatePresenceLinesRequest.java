package coo.app.baseline.fax.definitions;


public class UpdatePresenceLinesRequest
{
    //
    public PresenceLineInfo[] records;
    public UpdatePresenceLinesRequest records(PresenceLineInfo[] records) {
        this.records = records;
        return this;
    }
}
