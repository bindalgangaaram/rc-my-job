package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.End;

public class Meeting extends Path {
    public Meeting (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "meeting", id);
    }
    public com.ringcentral.paths.End end()
    {
        return new End(restClient, pathSegment, null);
    }
}
