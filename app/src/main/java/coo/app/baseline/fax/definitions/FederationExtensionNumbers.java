package coo.app.baseline.fax.definitions;


public class FederationExtensionNumbers
{
    // List of extension numbers of the current federation
    public FederationExtensionNumbersExtensionInfo[] records;
    public FederationExtensionNumbers records(FederationExtensionNumbersExtensionInfo[] records) {
        this.records = records;
        return this;
    }
}
