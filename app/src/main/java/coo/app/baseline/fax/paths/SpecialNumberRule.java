package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;

public class SpecialNumberRule extends Path {
    public SpecialNumberRule (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "special-number-rule", id);
    }
}
