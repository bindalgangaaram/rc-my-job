package coo.app.baseline.fax.paths;

import com.ringcentral.Path;
import com.ringcentral.PathSegment;
import com.ringcentral.RestClient;
import com.ringcentral.paths.Banners;

public class ClientInfo extends Path {
    public ClientInfo (RestClient restClient, PathSegment parent, String id) {
        this.restClient = restClient;
        pathSegment = new PathSegment(parent, "client-info", id);
    }
    public SipProvision sipProvision()
    {
        return new SipProvision(restClient, pathSegment, null);
    }
    public com.ringcentral.paths.Banners banners()
    {
        return new Banners(restClient, pathSegment, null);
    }
    public CustomData customData(String id)
    {
        return new CustomData(restClient, pathSegment, id);
    }
    public SpecialNumberRule specialNumberRule()
    {
        return new SpecialNumberRule(restClient, pathSegment, null);
    }
}
