package coo.app.baseline.fax.definitions;
import com.ringcentral.definitions.AddressFormCountryResource;

public class AddressFormResource
{
    //
    public String uri;
    public  AddressFormResource uri(String uri) {
        this.uri = uri;
        return this;
    }
    //
    public AddressFormCountryResource[] records;
    public  AddressFormResource records(AddressFormCountryResource[] records) {
        this.records = records;
        return this;
    }
}
