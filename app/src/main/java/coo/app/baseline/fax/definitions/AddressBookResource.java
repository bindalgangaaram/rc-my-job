package coo.app.baseline.fax.definitions;
import com.ringcentral.definitions.ResourceLink;

public class AddressBookResource
{
    //
    public String uri;
    public  AddressBookResource uri(String uri) {
        this.uri = uri;
        return this;
    }
    //
    public  ResourceLink contacts;
    public  AddressBookResource contacts(com.ringcentral.definitions.ResourceLink contacts) {
        this.contacts = contacts;
        return this;
    }
    //
    public  ResourceLink groups;
    public  AddressBookResource groups(ResourceLink groups) {
        this.groups = groups;
        return this;
    }
}
