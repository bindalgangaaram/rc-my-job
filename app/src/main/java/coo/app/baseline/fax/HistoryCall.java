package coo.app.baseline.fax;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import coo.app.baseline.fax.POJO.RecordCall;

public class HistoryCall extends AppCompatActivity {
    RecyclerView recyclerView;
     DishAdapter dAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history_call);
        Intent intent = getIntent();
        System.out.print(MainActivity.recordListCall);
        recyclerView = (RecyclerView) findViewById(R.id.call_history);
        initViews();
    }

    private void initViews() {
        dAdapter = new DishAdapter(getApplicationContext(),MainActivity.recordListCall);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(dAdapter);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
    }
    class DishAdapter extends RecyclerView.Adapter<DishAdapter.DishViewHolder> {
        private List<RecordCall> dishList;
        private Context context;
        public class DishViewHolder extends RecyclerView.ViewHolder {
            public TextView tet1_old, tet2_old, tet3_old,tet4_old;
            public Button btn_old_view;
            public DishViewHolder(View view) {
                super(view);
                tet1_old = (TextView) view.findViewById(R.id.tet1_old);
                tet2_old = (TextView) view.findViewById(R.id.tet2_old);
                tet3_old = (TextView) view.findViewById(R.id.tet3_old);
                tet4_old = (TextView) view.findViewById(R.id.tet4_old);
            }
        }
        public DishAdapter(Context context, List<RecordCall> dishList) {
            this.context = context;
            this.dishList = dishList;
        }
        private DishViewHolder holder;
        @Override
        public DishViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.callhistory, parent, false);
            return new DishViewHolder(itemView);
        }
        @Override
        public long getItemId(int position) {
            return position;
        }
        @Override
        public void onBindViewHolder(DishViewHolder holder, int position) {
            final RecordCall dish = dishList.get(position);
            holder.tet1_old.setText(dish.getToCall().getPhoneNumber());

            DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS", Locale.ENGLISH);
            DateFormat outputFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date formattedDate = null;
            try {
                formattedDate = inputFormat.parse(dish.getStartTime().toString());
            } catch (ParseException e) {
                e.printStackTrace();
            }
            String formattedDateStr = outputFormat.format(formattedDate);
            holder.tet2_old.setText(formattedDateStr);
            String string = dish.getStartTime().toString();
            String[] parts = string.split("T");
            String part1 = parts[0]; // 004
            String part2 = parts[1];
            String[] partss = part2.split(":");
            String time =  partss[0]+":"+ partss[1];
            holder.tet3_old.setText(time);
            holder.tet4_old.setText(dish.getDuration().toString());
        }
        @Override
        public int getItemCount() {
            return dishList.size();
        }
    }
    @Override
    public void onBackPressed() {
        MainActivity.recordListCall.clear();
        super.onBackPressed();
    }

}