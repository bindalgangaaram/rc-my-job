package coo.app.baseline.fax.definitions;

public class CustomGreetingAnsweringRuleInfoRequest
{
    // Internal identifier of an answering rule
    public String id;
    public CustomGreetingAnsweringRuleInfoRequest id(String id) {
        this.id = id;
        return this;
    }
}
