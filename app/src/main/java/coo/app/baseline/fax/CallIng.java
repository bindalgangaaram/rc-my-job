//package coo.app.baseline.fax;
//
//import android.content.Intent;
//import android.os.AsyncTask;
//import android.support.v7.app.AppCompatActivity;
//import android.os.Bundle;
//import android.view.View;
//import android.widget.Button;
//import android.widget.TextView;
//
//import com.ringcentral.*;
//
//import java.io.IOException;
//
//import coo.app.baseline.fax.definitions.CallLogCallerInfo;
//import coo.app.baseline.fax.definitions.RingOutInfo;
//import coo.app.baseline.fax.definitions.RingOutResource;
//
//import static junit.framework.Assert.assertNotNull;
//
//public class CallIng extends AppCompatActivity {
//    RingOutInfo ringOutResource;
//    TextView textView_status;
//    Button cencel;
//    RestClient restClient;
//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_call_ing);
//        Intent intent = getIntent();
//        ringOutResource = (RingOutInfo) intent.getSerializableExtra("ringOutResource");
//        restClient = (RestClient) intent.getSerializableExtra("restClient");
//        textView_status = (TextView)findViewById(R.id.status_call);
//        textView_status.setText(ringOutResource.status.toString());
//        cencel = (Button)findViewById(R.id.cencel);
//        cencel.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//            }
//        });
//    }
//    public class Send_READ_CALL_LOG extends AsyncTask<Object, Object, String> {
//        @Override
//        protected String doInBackground(Object... objects) {
//            CallLogCallerInfo callLogCallerInfo = null;
//            try {
//                callLogCallerInfo = restClient.get("/restapi/v1.0/account/~/extension/~/call-log", CallLogCallerInfo.class);
//            } catch (IOException e) {
//                e.printStackTrace();
//            } catch (com.ringcentral.RestException e) {
//                e.printStackTrace();
//            }
//            assertNotNull(callLogCallerInfo);
//            return null;
//        }
//    }
//}
