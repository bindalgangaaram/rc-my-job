package coo.app.baseline.fax.definitions;


public class ScheduleInfoUserBusinessHours
{
    // Weekly schedule
    public WeeklyScheduleInfo weeklyRanges;
    public ScheduleInfoUserBusinessHours weeklyRanges(WeeklyScheduleInfo weeklyRanges) {
        this.weeklyRanges = weeklyRanges;
        return this;
    }
}
